# Generated by Django 3.2.7 on 2021-10-01 12:45

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Note',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('to', models.CharField(max_length=12)),
                ('fr', models.CharField(max_length=12)),
                ('title', models.CharField(max_length=25)),
                ('message', models.CharField(max_length=100)),
            ],
        ),
    ]
