from django.db import models

# Create your models here.

class Note(models.Model):
    to = models.CharField(max_length = 12)
    fr = models.CharField(max_length = 12)
    title = models.CharField(max_length = 25)
    message = models.CharField(max_length = 100)
        