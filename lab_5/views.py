from django.shortcuts import render, get_object_or_404, HttpResponseRedirect
from django.core import serializers
from lab_2.models import Note
from .forms import NoteForm
from django.http.response import HttpResponse
import json
from django.forms.models import model_to_dict

# Create your views here.

def index(request):
    notes = Note.objects.all().values()
    response = {'notes' : notes}
    return render(request, 'lab5_index.html', response)

def get_note(request, id):
    data = model_to_dict(Note.objects.get(id=id))
    return HttpResponse(json.dumps(data), content_type = "application/json", status = 200)
    

def update_note(request, id):
    obj = get_object_or_404(Note, id = id)
    form = NoteForm(request.POST or None, instance = obj)
    if form.is_valid():
        form.save()
    data = serializers.serialize('json', form)
    return HttpResponse(data, content_type = "application/json", status = 200)

def delete_note(request, id):
    obj = get_object_or_404(Note, id = id)
    if request.method =="POST":
        obj.delete()
    return HttpResponse(content_type = "application/json", status = 200)
