class Note {
  String to;
  String from;
  String title;
  String message;

  Note({
    this.to,
    this.from,
    this.title,
    this.message,
  });
}
