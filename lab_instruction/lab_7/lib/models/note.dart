class Note {
  String to;
  String from;
  String title;
  String message;

  Note({
    required this.to,
    required this.from,
    required this.title,
    required this.message,
  });
}
