import 'package:flutter/material.dart';

class MyCard extends StatefulWidget {
  const MyCard({Key? key}) : super(key: key);

  @override
  _MyCardState createState() => _MyCardState();
}

class _MyCardState extends State<MyCard> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Card(
          color: Colors.greenAccent,
          child: Column(
            children: [
              ListTile(
                  title: Text(
                    'To: Ani',
                    style: TextStyle(
                      color: Colors.black.withOpacity(0.6),
                      fontWeight: FontWeight.bold,
                      fontSize: 30,
                    ),
                  ),
                  subtitle: Column(
                    children: [
                      Align(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          'From: Budi',
                          style: TextStyle(
                            color: Colors.black.withOpacity(0.6),
                            fontSize: 20,
                          ),
                        ),
                      ),
                      Align(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          'Title: Riddle Sekolah Tua',
                          style: TextStyle(
                            color: Colors.black.withOpacity(0.6),
                            fontSize: 20,
                          ),
                        ),
                      )
                    ],
                  )
              ),
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: Text(
                  'Halo, Ani! Apa kabar? Udah lama nih kita ga main riddle gara-gara di rumah aja. Ayo kita main riddle lagi! Online aja juga gapapa yang penting seru. Okee deh. Aku mulai duluan, ya. Nih riddle-nya. Tiap kami pulang, kami selalu melewati sebuah sekolah tua yang terbengkalai. Bangunannya sudah tertutup rapat agar tidak ada yang masuk. Kayu-kayunya sudah lapuk, namun jendela kacanya masih utuh. Tiap kali kami lewat selalu saja ada cap telapak tangan yang menempel di jendelanya. Dasar anak-anak usil, pikirku. Tempat seseram ini tapi masih saja mereka bermain di sini. Lagian ini kan musim dingin. Menggigil begini masih saja mereka main di luar.',
                  style: TextStyle(
                    color: Colors.black.withOpacity(0.6),
                  ),
                ),
              ),
              const Divider(
                height: 5.0,
              ),
            ],
          ),
        ),

        Card(
          color: Colors.deepOrangeAccent,
          child: Column(
            children: [
              ListTile(
                  title: Text(
                    'To: Budi',
                    style: TextStyle(
                      color: Colors.black.withOpacity(0.6),
                      fontWeight: FontWeight.bold,
                      fontSize: 30,
                    ),
                  ),
                  subtitle: Column(
                    children: [
                      Align(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          'From: Ani',
                          style: TextStyle(
                            color: Colors.black.withOpacity(0.6),
                            fontSize: 20,
                          ),
                        ),
                      ),
                      Align(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          'Title: Ada Orang di Dalam!',
                          style: TextStyle(
                            color: Colors.black.withOpacity(0.6),
                            fontSize: 20,
                          ),
                        ),
                      )

                    ],
                  )
              ),
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: Text(
                  'Haloo, Budi! Kabarku baik. Kamu gimana? Sehat-sehat aja kan. Iyanih, udah lama banget kita ga main riddle. Ayo main! Serem juga ya riddle-nya. Cap telapak tangannya berasal dari dalam, karena saat musim dingin suhu di dalam lebih hangat dan jendela akan berembun. Tapi pertanyaannya, Budi, siapa yang ada di dalem kalo sekolahnya aja udah lama ditutup?',
                  style: TextStyle(color: Colors.black.withOpacity(0.6)),
                ),
              ),
              const Divider(
                height: 5.0,
              ),
            ],
          ),
        ),
      ],
    );
  }
}
