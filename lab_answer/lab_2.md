1. Apakah perbedaan JSON dan XML?
    Data pada JSON disimpan dalam bentuk key dan value, sedangkan pada XML, data dibungkus dalam tag.
    JSON mendukung penggunaan array, sedangkan XML tidak.
    JSON sangat lebih human readable daripada XML.

2. Apakah perbedaan antara HTML dan XML?
    HTML memiliki tag yang sudah ditentukan, sedangkan pada XML, tag dibuat sendiri.
    HTML digunakan pada aplikasi web, sedangkan XML digunakan pada aplikasi web dan mobile.
    Pada HTML, terdapat elemen yang tidak memiliki end tag disebut sebagai empty elemen, sedangkan pada XML, semua elemen memiliki end tag.
    Tag pada HTML bersifat case-insensitive, sedangkan pada XML bersifat case-sensitive.